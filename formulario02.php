<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Procesar PHP dentro del mismo formulario</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    
    <script src="js/jquery-1.12.1.min.js"></script>
    <script src="bootstrap.min.js"></script>
    <style>
        div#cuerpo{
            margin: auto;
            width: 220px;
            text-align:left;
            margin-top: 40px;
            border: 3px solid black;
            background-color: #787878;
        }
        #table{
        text-align: left;
        }
    </style>
</head>
<body>
    <div id="cuerpo">
<table id="table" class="table table-bordered table-hover">
    <form action="#" method="POST" >
    
        <legend class="btn btn-default">Calculadora</legend>
        <p></p>
        <tr>
        <p class="btn btn-primary">Nro1: <input type="text" name="txtNro1" /></p><br>
        <p class="btn btn-primary">Nro2: <input type="text" name="txtNro2"/></p>
        <p>

        </p>
        </tr>
        </tr>
        <p><input type="submit" name="btnSumar" class="btn btn-primary" value="Sumar"/> 
        <input type="submit" name="btnRestar" class="btn btn-primary" value="Restar"/> 
        <input type="submit" name="btnMutiplicar"class="btn btn-primary"  value="Multiplicar"/> 
        </tr>
        </tr>
        <input type="submit" name="btnDividir" class="btn btn-primary" value="Dividir"/> 
        <input type="submit" name="btnPotencia" class="btn btn-primary" value="Potencia"/> 
        <input type="submit" name="btnTangente" class="btn btn-primary" value="Tangente"/>
        </tr>
        </tr>
        <input type="submit" name="btnSeno" class="btn btn-primary"  value="Seno"/> 
        <input type="submit" name="btnPorcentaje"class="btn btn-primary"  value="Porcentaje"/> 
        <input type="submit" name="btnRaizcuadrada" class="btn btn-primary" value="Raiz Cuadrada"/> 
        </tr>
        </tr>
        <input type="submit" name="btnRaizn" class="btn btn-primary" value="Raiz n-esima"/> 
        <input type="submit" name="btnInversa" class="btn btn-primary" value="Inverso"/> 
        </tr>
        </p>
        </table></div>
    </form>    
    <?php 
    if(isset($_POST))
    {
        // Llamar a la clase Calculadora
        include("calculadora.php");        
        $nro1 = $_POST['txtNro1'];
        $nro2 = $_POST['txtNro2'];
        if(isset($_POST['btnSumar']))
        {
            // Instanciar un objeto a traves de la clase
            $calculo = new Calculadora;
            $calculo->nro1 = $nro1;
            $calculo->nro2 = $nro2;
            $suma = $calculo->Sumar();
            echo "La suma de los numeros es: " , $suma;
        }
        else  if(isset($_POST['btnRestar']))
        {
            $calculo = new Calculadora;
            $calculo->nro1 = $nro1;   
            $calculo->nro2 = $nro2;            
            $resta = $calculo->Restar();
            echo "La resta es: " , $resta;
        }        
        else  if(isset($_POST['btnDividir']))
        {
            $calculo = new Calculadora;
            $calculo->nro1 = $nro1;   
            $calculo->nro2 = $nro2;            
            $cociente = $calculo->Dividir();
            echo "La division es: " , $cociente;
        }
        else  if(isset($_POST['btnMutiplicar']))
        {
            $calculo = new Calculadora;
            $calculo->nro1 = $nro1;   
            $calculo->nro2 = $nro2;            
            $prod = $calculo->Multiplicar();
            echo "La multi es: " , $prod;
        }  
        else  if(isset($_POST['btnPotencia']))
        {
            $calculo = new Calculadora;
            $calculo->nro1 = $nro1;   
            $calculo->nro2 = $nro2;            
            $pot = $calculo->Potencia();
            echo "La potencia es: " , $pot;
        }
        else  if(isset($_POST['btnTangente']))
        {
            $calculo = new Calculadora;
            $calculo->nro1 = $nro1;             
            $tg = $calculo->Tangente($nro1);
            echo "La tangente es: " , $tg;
        }
        else  if(isset($_POST['btnSeno']))
        {
            $calculo = new Calculadora;
            $calculo->nro1 = $nro1;             
            $sen = $calculo->Seno($nro1);
            echo "El seno es: " , $sen;
        }
        else  if(isset($_POST['btnPorcentaje']))
        {
            $calculo = new Calculadora;
            $calculo->nro1 = $nro1; 
            $calculo->nro2 = $nro2;
            $porc = $calculo->Porcentaje();
            echo "El porcentaje del numero es: " , $porc;
        }
        else  if(isset($_POST['btnRaizcuadrada']))
        {
            $calculo = new Calculadora;
            $calculo->nro1 = $nro1; 
            $rc = $calculo->RaizCuadrada($nro1);
            echo "La raiz cuadrada es: " , $rc;
        }
        else  if(isset($_POST['btnRaizn']))
        {
            $calculo = new Calculadora;
            $calculo->nro1 = $nro1; 
            $calculo->nro2 = $nro2;
            $rn = $calculo->RaizN();
            echo "La raiz es: " , $rn;
        }
        else  if(isset($_POST['btnInversa']))
        {
            $calculo = new Calculadora;
            $calculo->nro1 = $nro1;
            $in = $calculo->Inversa($nro1);
            echo "La inversa es: " , $in;
        }
    }
    
    ?>
</body>
</html>
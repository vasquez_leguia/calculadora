<?php
class Calculadora {
    //atributos
    public $nro1;
    public $nro2;
    //metodos u operaciones
    public function Sumar(){
        return $this->nro1 + $this->nro2;
    }
    public function Restar(){
        return $this->nro1 - $this->nro2;
    }
    //multiplicar y dividir
    public function Multiplicar(){
        return $this->nro1 * $this->nro2;
    }
    public function Dividir(){
        if($this->nro2==0)
        return "Error";
        else
        return $this->nro1/$this->nro2;
    }
    public function Factorial($nro1)
    {
        if ($nro1==1)return 1;
        else
        return $this->nro1*Factorial($this->nro1-1);
    }
    //Potencia
    public function Potencia(){
    if($this->nro2==0)
    return 1;
    else
    return pow($this->nro1, $this->nro2);
    }
    //seno
    public function Seno($nro1)
    {
        return (sin(deg2rad($this->nro1)));
    }
    //tangente
    public function Tangente($nro1){
    return (tan(($this->nro1 * pi())/180));
    }
    //Porcentaje
    public function Porcentaje(){
        return ($this->nro1/100)*$this->nro2;
    }
    //Raiz cuadrada
    public function RaizCuadrada($nro1){
    return sqrt($this->nro1);
    }
    //Raiz n-sima
    public function RaizN(){
    return pow($this->nro1, (1/$this->nro2));
    }
    //inversa
    public function Inversa($nro1){
        return 1/$this->nro1;
    }
}
?>